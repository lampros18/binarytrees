#ifndef _TREES_H
#define _TREES_H

#define TRUE 1
#define FALSE 0

typedef int elem;

struct node
{
  elem data;
  struct node *left;
  struct node *right;
};

typedef struct node TREE_NODE;
typedef struct node *TREE_PTR;

void init(TREE_PTR *root);
int isEmpty(TREE_PTR root);
elem data(TREE_PTR p);
int insert_root(TREE_PTR *root,elem x);
int insert_left(TREE_PTR node,elem x);
int insert_right(TREE_PTR node,elem x);
int delete_root(TREE_PTR *root,elem *x);
int delete_left(TREE_PTR node,elem *x); //node == parent
int delete_right(TREE_PTR node,elem *x); //node == parent
void preorder(TREE_PTR v);
void inorder(TREE_PTR v);
void postorder(TREE_PTR v);
void print(TREE_PTR v);

#endif
