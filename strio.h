#ifndef STRIO_STRIO_H
#define STRIO_STRIO_H

#define MAXSIZE 1024
#define MAXROWS 2

int clean_stdin(void);
void save_string ( char *string , size_t size );
void save_string_2d(char string[MAXROWS][MAXSIZE]);

size_t input ( char *string , size_t size , char * string1);
void input_2d( char string[MAXROWS][MAXSIZE] , size_t size , size_t size1 ,char *string1);
short load_string(char *string , size_t size);
short load_2d(char string[MAXROWS][MAXSIZE]);
void print_2d(char string[MAXROWS][MAXSIZE]);
#endif //STRIO_STRIO_H
