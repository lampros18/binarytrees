#include <stdio.h>
#include <stdlib.h>
#include "BinaryTrees.h"
#include "strio.h"

unsigned int insertStudent(TREE_PTR *root);
int main(int argc, char const *argv[]) {

  TREE_PTR root;
  init(&root);

  unsigned int run = 1;
  int choice;
  int id;
  system("clear");
  while (run) {
    printf("%s","\n1-Insert Client\n2-Delete Client\n3-Clear the screen\n4-Print\n5-Exit\n" );
    scanf("%d",&choice );
    switch (choice) {
      case 1:
      insertStudent(&root);
      break;
      case 2:
      puts("Please provide me the id of the student you want to delete");
      scanf("%d",&id );
      struct student tmp = { "" , "" , id };
      deleteNode(root,tmp);
      break;
      case 3:
      system("clear");
      break;
      case 4:
      inorder(root);
      break;
      case 5:
      run = 0;
      destroy_tree(root);
      break;
      default:
      puts("Invalid Choice");
      break;
    }
  }
  return 0;
}

unsigned int insertStudent(TREE_PTR *root)
{
  struct student student1;
  printf("%s", "Please enter the id: ");
  scanf("%d",&student1.id );
  clean_stdin();
  input(student1.name,30,"Please enter the name: ");
  input(student1.surname,30,"Please enter the surname: ");
  insert(root,student1);
}
