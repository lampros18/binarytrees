#include "Trees.h"
#include <stdio.h>
int main(int argc, char const *argv[]) {

  TREE_PTR root;
  init(&root);
  insert_root(&root,10);
  insert_left(root,6);//
  insert_right(root,14);//
  insert_left(root,5);//
  insert_right(root,7);//
  insert_right(root,19);
  insert_left(root,2);
  insert_right(root,8);
  insert_right(root,3);
  puts("preorder:");
  preorder(root);
  puts("");
  puts("inorder:");
  inorder(root);
  puts("");
  puts("postorder:");
  postorder(root);
  puts("");
  return 0;
}
