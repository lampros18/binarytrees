#include "BinaryTrees.h"
#include <stdio.h>
#include <stdlib.h>

void init(TREE_PTR *root)
{
  *root = NULL;
}

void destroy_tree(TREE_PTR leaf)
{
  if( leaf != 0 )
  {
      destroy_tree(leaf->left);
      destroy_tree(leaf->right);
      free( leaf );
  }
}

void insert(TREE_PTR *leaf, elem key)
{
    if( *leaf == 0 )
    {
        *leaf = (TREE_PTR) malloc( sizeof( TREE_NODE ) );
        (*leaf)->key_value = key;
        /* initialize the children to null */
        (*leaf)->left = 0;
        (*leaf)->right = 0;
    }
    else if(key.id < (*leaf)->key_value.id)
    {
        insert( &(*leaf)->left, key );
    }
    else if(key.id > (*leaf)->key_value.id)
    {
        insert(&(*leaf)->right,  key );
    }
}

void preorder(TREE_PTR v)
{
  if( v != NULL )
  {
    print(v); // Head Recursion
    preorder(v->left);
    preorder(v->right);
  }
}

void inorder(TREE_PTR v)
{
  if( v != NULL )
  {
    inorder(v->left);
    print(v);
    inorder(v->right);
  }

}

void postorder(TREE_PTR v)
{
  if( v != NULL )
  {
    postorder(v->left);
    postorder(v->right);
    print(v);
  }
}
TREE_PTR search(elem key, TREE_PTR leaf)
{
  if( leaf != 0 )
  {
      if(key.id==leaf->key_value.id)
      {
          return leaf;
      }
      else if(key.id < leaf->key_value.id)
      {
          return search( key, leaf->left );
      }
      else
      {
          return search( key, leaf->right );
      }
  }
  else return 0;
}

// int deleteBST( TREE_PTR *root , elem x)
// {
//   TREE_PTR current,parent,nextOrdered;
//   int p;
//   //search
//
//   //variable initialization
//   parent = NULL;
//   current = *root;
//
//   while ( current != NULL ) {
//     /* code */
//     if( x = current -> data )
//     {
//       break;
//     }else if( x < current -> data)
//     {
//       parent = current;
//       p = 1;
//       current = current -> left;
//     }else if ( x > current -> data)
//     {
//       parent = current; // parent
//       p = 2;
//       current = current -> right; // here the pointer shows to the child(a way to look a tree before)
//     }
//   }
//
//   if (current == NULL)
//   return 0;
//
//   if ( current -> left == NULL && current -> right == NULL) //The node hasn't any children
//   {
//     free(current);
//     if( parent == NULL )
//       *root = NULL; // removed the root of the tree !!
//     else if ( p == 1)
//       parent -> left = NULL;
//       else
//         parent -> right = NULL;
//         return 1;
//   }else if ( current -> left != NULL && current -> right == NULL) //has one child at left
//   {
//     if(parent == NULL)
//       *root = current -> left;
//       else if ( p == 1 )
//         parent -> left = current -> left;
//         else
//           parent -> right = current -> left;
//         free(current);
//         return 1;
//   }else if ( current -> left == NULL && current -> right == NULL) //has one child at right
//   {
//     if(parent == NULL)
//       *root = current -> right;
//       else if ( p == 1)
//         parent -> left = current -> right;
//         else
//           parent -> right =  current -> right;
//         free(current);
//         return 1;
//   }else // has two children
//   {
//     p=1;
//     //find the next inorder
//     nextOrdered = current -> right;
//
//     while ( nextOrdered -> left != NULL ) {
//       parent = nextOrdered;
//       nextOrdered = nextOrdered -> left;
//       p = 2;
//     }
//     current -> data = nextOrdered -> data;
//     if ( p == 1 )
//     {
//       current -> right =  nextOrdered -> right;
//       free(nextOrdered);
//     }else{
//       parent -> left = nextOrdered -> right;
//       free(nextOrdered);
//     }
//     }
//   }
// }
//
// /* Given a non-empty binary search tree, return the node with minimum
//    key value found in that tree. Note that the entire tree does not
//    need to be searched. */
TREE_PTR minValueNode(TREE_PTR node) //necessary for the deleteNode function
{
    TREE_PTR current = node;

    /* loop down to find the leftmost leaf */
    while (current->left != NULL)
        current = current->left;

    return current;
}

struct node* deleteNode(struct node* root, elem key)
{
    // base case
    if (root == NULL) return root;

    // If the key to be deleted is smaller than the root's key,
    // then it lies in left subtree
    if (key.id < root->key_value.id)
        root->left = deleteNode(root->left, key);

    // If the key to be deleted is greater than the root's key,
    // then it lies in right subtree
    else if (key.id > root->key_value.id)
        root->right = deleteNode(root->right, key);

    // if key is same as root's key, then This is the node
    // to be deleted
    else
    {
        // node with only one child or no child
        if (root->left == NULL)
        {
            struct node *temp = root->right;
            free(root);
            return temp;
        }
        else if (root->right == NULL)
        {
            struct node *temp = root->left;
            free(root);
            return temp;
        }

        // node with two children: Get the inorder successor (smallest
        // in the right subtree)
        struct node* temp = minValueNode(root->right);

        // Copy the inorder successor's content to this node
        root->key_value = temp->key_value;

        // Delete the inorder successor
        root->right = deleteNode(root->right, temp->key_value);
    }
    return root;
}

void print(TREE_PTR v)
{
  printf("%s\n","node{" );
  printf("%d -> id\n ", v -> key_value.id );
  printf("%s -> name\n ", v -> key_value.name );
  printf("%s -> surname\n ", v -> key_value.surname );
  printf("%s\n","}" );
}

// int searchBST(TREE_PTR root,elem x)
// {
//   TREE_PTR current;
//
//   current = root;
//   while(current != NULL)
//   {
//     if(x == current -> key_value.id)
//     {
//       return 1;
//     }else if( x < current-> key_value.id){
//       current = current -> left;
//     }else if(x > current -> key_value.id)
//     {
//       current = current -> right;
//     }
//   }
//   return 0;//FALSE
//
// }
