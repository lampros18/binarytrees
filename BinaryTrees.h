#ifndef _BINARY_TREES_H
#define _BINARY_TREES_H

#define TRUE 1
#define FALSE 0

struct student
{
  char name[30];
  char surname[30];
  int id;
};


typedef struct student elem;
struct node
{
  elem key_value;
  struct node *left;
  struct node *right;
};

typedef struct node TREE_NODE;
typedef struct node * TREE_PTR;

void init(TREE_PTR *root);
void destroy_tree(TREE_PTR leaf);
void insert(TREE_PTR *leaf, elem key);
TREE_PTR search(elem key, TREE_PTR leaf);

void preorder(TREE_PTR v);
void inorder(TREE_PTR v);
void postorder(TREE_PTR v);
void print(TREE_PTR v);
struct node *search(elem key, struct node *leaf);
struct node* deleteNode(struct node* root, elem key);
TREE_PTR minValueNode(TREE_PTR node);


#endif
