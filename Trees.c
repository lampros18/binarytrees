#include "Trees.h"
#include <stdio.h>
#include <stdlib.h>

void init(TREE_PTR *root)
{
  *root=NULL;
}

int isEmpty(TREE_PTR root)
{
  return root == NULL;
}

elem data(TREE_PTR p)
{
  return p->data;
}

int insert_root(TREE_PTR *root,elem x)
{
  TREE_PTR newnode;

  newnode = (TREE_PTR)malloc(sizeof(TREE_NODE));

  if(newnode == NULL)
  {
    puts("Memory allocation failed!");
    return FALSE;
  }

  newnode -> data = x;
  newnode -> left = NULL;
  newnode -> right = NULL;

  *root = newnode;

  return TRUE;
}

int insert_left(TREE_PTR node,elem x)
{
  TREE_PTR newnode;
  TREE_PTR *traversePtr = &(node -> left);

  newnode = (TREE_PTR)malloc(sizeof(TREE_NODE));

  if(newnode == NULL)
  {
    puts("Memory allocation failed!");
    return FALSE;
  }
  newnode -> data = x;
  newnode -> left = NULL;
  newnode -> right = NULL;
  TREE_PTR tmp = node -> left;
  if(tmp != NULL){
    while(1)
    {
      if(tmp -> left == NULL)
        break;
      tmp = tmp -> left;
    }
      traversePtr = &(tmp -> left);
  }
    *traversePtr = newnode;
  return TRUE;
}

int insert_right(TREE_PTR node,elem x)
{
  TREE_PTR newnode;
  TREE_PTR *traversePtr = &(node -> right);

  newnode = (TREE_PTR)malloc(sizeof(TREE_NODE));

  if(newnode == NULL)
  {
    puts("Memory allocation failed!");
    return FALSE;
  }
  newnode -> data = x;
  newnode -> left = NULL;
  newnode -> right = NULL;

  TREE_PTR tmp = node -> right;
  if(tmp != NULL){
    while(1)
    {
      if(tmp -> right == NULL)
        break;
      tmp = tmp -> right;
    }
      traversePtr = &(tmp -> right);
  }
    *traversePtr = newnode;
  return TRUE;
}

int delete_root(TREE_PTR *root,elem *x)
{
  if( (*root) -> left != NULL || (*root) -> right != NULL)
  {
    return FALSE;
  }
  *x=(*root)->data;
  free(*root);
  *root = NULL;
  return TRUE;
}

int delete_left(TREE_PTR node,elem *x) //node == parent
{

  if(node -> left == NULL)
  {
    return FALSE;
  }

  if( node -> left -> left != NULL || node -> left -> right != NULL)
  {
    return FALSE;
  }
  *x = node -> left -> data;
  free(node -> left);
  node -> left = NULL;
  return  TRUE;
}

int delete_right(TREE_PTR node,elem *x) //node == parent
{

  if(node -> right == NULL)
  {
    return FALSE;
  }

  if( node -> right -> left != NULL || node -> right -> right != NULL)
  {
    return FALSE;
  }
  *x = node -> right -> data;
  free(node -> right);
  node -> right = NULL;
  return TRUE;
}

void preorder(TREE_PTR v)
{
  if( v != NULL )
  {
    print(v); // Head Recursion
    preorder(v->left);
    preorder(v->right);
  }
}

void inorder(TREE_PTR v)
{
  if( v != NULL )
  {
    inorder(v->left);
    print(v); // Head Recursion
    inorder(v->right);
  }
}

void postorder(TREE_PTR v)
{
  if( v != NULL )
  {
    inorder(v->left);
    inorder(v->right);
    print(v); // Head Recursion
  }
}

void print(TREE_PTR v)
{
  printf("%d ",data(v) );
}
